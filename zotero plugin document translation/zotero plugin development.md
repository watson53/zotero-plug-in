_This page is a work in progress._

## Introduction to Zotero Plugins

Zotero plugins run within the Zotero desktop app and are built on the same technologies as legacy (pre-WebExtension) Firefox extensions and interact with Zotero's internal [JavaScript API](https://www.zotero.org/support/dev/client_coding/javascript_api "dev:client_coding:javascript_api").

If you plan to write a plugin, you can start by taking a look at [existing plugins](https://www.zotero.org/support/plugins "plugins") such as [ZotFile](https://github.com/jlegewie/zotfile "https://github.com/jlegewie/zotfile"). (The official [Hello World Zotero plugin](https://www.zotero.org/support/dev/sample_plugin "dev:sample_plugin") is currently extremely dated.)

## Alternatives to Zotero Plugins

## Setting Up a Plugin Development Environment

When developing a Zotero client plugin, it's helpful to have Zotero run the plugin directly from source. After creating your plugin's source directory with sample code, you can tell Zotero to load the plugin by creating an extension proxy file. (This is a technique that used to be possible for Firefox extension development, though it's since been discontinued in Firefox.)

1.  Close Zotero.
    
2.  Create a text file in the 'extensions' directory of your [Zotero profile directory](https://www.zotero.org/support/kb/profile_directory "kb:profile_directory") named after the extension id (e.g., myplugin@mydomain.org). The file contents should be the absolute path to the root of your plugin source code directory, where your install.rdf file is located.
    
3.  Open prefs.js in the Zotero profile directory in a text editor and delete the lines containing `extensions.lastAppBuildId` and `extensions.lastAppVersion`. Save the file and restart Zotero. This will force Zotero to read the 'extensions' directory and install your plugin from source, after which you should see it listed in Tools → Add-ons. This is only necessary once.
    
4.  Whenever you start up Zotero after making a change to your extension code, start it from the command line and pass the `-purgecaches` flag to force Zotero to re-read any cached files. (You'll likely want to make an alias or shell script that also includes the `-ZoteroDebugText` and `-jsconsole` flags and perhaps `-p <Profile>`, where `<Profile>` is the name of a development profile.)
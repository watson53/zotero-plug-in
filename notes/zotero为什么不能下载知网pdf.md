zotero的浏览器抓取功能是不同的网页都有对应的脚本，里面用js语言写了该网页的解析规则，因此要实现对当前网页的抓取功能就需要有这个网页的脚本才行。

zotero浏览器抓取的脚本在zotero数据库的translators文件夹里面，如下图所示。

<img src="zotero为什么不能下载知网pdf.assets/image-20220826161638897.png" alt="image-20220826161638897"  />

如果你需要抓取知网，就需要该网页的js脚本，可以到该网址下载https://gitee.com/l0o0/translators_CN。

![image-20220826162253483](zotero为什么不能下载知网pdf.assets/image-20220826162253483.png)

下载好的文件解压后将里面的translators文件夹里面的js文件复制到前面提到的zotero数据库的translators文件夹。重启zotero，重启浏览器。不行就多试几次。

zotero抓取的脚本里面如果有解析pdf地址的功能，就会根据拿到的地址去下载pdf，有的人会发现你能够在浏览器里面手动下载pdf，但是zotero不能下载pdf，有可能是因为你的浏览器使用的是学校的vpn，但是zotero抓取的时候可没有vpn功能可用，当然在向知网请求pdf的时候被拒绝

还有人发现刚才还能下载，现在下载不了了，这种情况有可能是你下载的pdf过多被知网判定为机器人，不予下载，这时候就手动下载pdf，拖到zotero对应的条目里面去就行了。

综上：zotero能抓取的条目要求有对应的解析脚本，pdf要求你的浏览器可以在不挂校园vpn的情况下直接下载，如果你是走学校的链接进入知网的，大概率是vpn，可以看下网址里面有没有vpn字样，一般的知网网址是(https://www.cnki.net/)这样的，其他形式zotero的js脚本都不能识别